from django.contrib import admin
from main.models import *

admin.site.register(Hospital)
admin.site.register(HospitalAdmin)
admin.site.register(Doctor)
admin.site.register(Prescription)
