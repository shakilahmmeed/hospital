from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.contrib import messages
from django.contrib.auth.models import User, auth
from django.contrib.auth.decorators import login_required
from main.models import *


def index(request):
    return render(request, 'main/index.html')


def for_doctor(request):
    return render(request, 'main/landing-doctor.html')


def for_hospital(request):
    return render(request, 'main/landing-hospital.html')


@login_required
def dashboard(request):
    user = User.objects.get(id=request.user.id)
    is_doctor = Doctor.objects.filter(user_id=request.user.id).exists()
    is_hospital_admin = HospitalAdmin.objects.filter(
        user_id=request.user.id).exists()

    if is_doctor:
        return render(request, 'main/dashboard-doctor.html')
    elif is_hospital_admin:
        hospital_admin = HospitalAdmin.objects.get(user=request.user)
        hospital_name = hospital_admin.hospital.name
        context = {
            'hospital_name': hospital_name
        }
        return render(request, 'main/dashboard-hospital.html', context)


def login_fn(request):
    if request.method == "GET":
        return render(request, 'main/login.html')

    elif request.method == "POST":
        username = request.POST["username"]
        password = request.POST["password"]

        user = auth.authenticate(username=username, password=password)

        if user is not None:
            auth.login(request, user)
            return redirect('dashboard')

        else:
            messages.error(request, 'Invalid Credentials!')
            return redirect('/login')


def logout_fn(request):
    auth.logout(request)
    return redirect("/")


def register_fn(request):
    forr = request.GET['for']
    if request.method == 'GET':
        if forr == "doctor":
            return render(request, 'main/register-doctor.html')
        elif forr == "hospital":
            return render(request, 'main/register-hospital.html')
    elif request.method == 'POST':
        if forr == 'doctor':
            firstName = request.POST["firstName"]
            lastName = request.POST["lastName"]
            username = request.POST["username"]
            email = request.POST["email"]
            password1 = request.POST["password1"]
            password2 = request.POST["password2"]

            if password1 != password2:
                messages.error(request, 'Passwords are not same!')
                return redirect('/register')

            elif User.objects.filter(username=username).exists():
                messages.error(request, 'Username Already taken!')
                return redirect('/register')

            elif User.objects.filter(email=email).exists():
                messages.error(
                    request, 'Already have an account with this email!')
                return redirect('/register')

            else:
                user = User.objects.create_user(
                    first_name=firstName, last_name=lastName, username=username, email=email, password=password1)
                doctor = Doctor.objects.create(user=user)
                return redirect('/login')

        elif forr == 'hospital':
            hospital_name = request.POST["hospital-name"]
            firstName = request.POST["firstname"]
            lastName = request.POST["lastname"]
            username = request.POST["username"]
            email = request.POST["email"]
            password1 = request.POST["password1"]
            password2 = request.POST["password2"]

            if password1 != password2:
                messages.error(request, 'Passwords are not same!')
                return redirect('/register')

            elif User.objects.filter(username=username).exists():
                messages.error(request, 'Username Already taken!')
                return redirect('/register')

            elif User.objects.filter(email=email).exists():
                messages.error(
                    request, 'Already have an account with this email!')
                return redirect('/register')

            else:
                user = User.objects.create_user(
                    first_name=firstName, last_name=lastName, username=username, email=email, password=password1)
                hospital = Hospital.objects.create(name=hospital_name)
                HospitalAdmin.objects.create(user=user, hospital=hospital)
                return redirect('/login')

            return HttpResponse('you posted for hospital')
