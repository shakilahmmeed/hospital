from django.db import models
from django.contrib.auth.models import User


class Hospital(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    contact_one = models.CharField(max_length=50)
    contact_two = models.CharField(max_length=50)
    contact_mail = models.EmailField(max_length=100)
    website = models.URLField(max_length=200)


class HospitalAdmin(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    hospital = models.ForeignKey(Hospital, on_delete=models.CASCADE)


class Doctor(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    designation = models.CharField(max_length=100, blank=True)
    contact = models.CharField(max_length=50, blank=True)
    hospital = models.ForeignKey(Hospital, on_delete=models.CASCADE, null=True)
    qualification = models.CharField(max_length=100, blank=True)


class Prescription(models.Model):
    GENDER_CHOICES = [
        ('male', 'Male'),
        ('female', 'Female'),
        ('others', 'Others'),
    ]
    name = models.CharField(max_length=50),
    gender = models.CharField(max_length=20, choices=GENDER_CHOICES)
    age = models.IntegerField(null=True)
    weight = models.CharField(max_length=20, blank=True)
    contact = models.CharField(max_length=20, blank=True)
    email = models.EmailField(max_length=50, blank=True)
    address = models.CharField(max_length=200, blank=True)
