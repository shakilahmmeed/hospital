from django.contrib import admin
from django.urls import path
from main import views as main_views


urlpatterns = [
    path('', main_views.index),
    path('register/', main_views.register_fn, name='register'),
    path('login/', main_views.login_fn),
    path('logout/', main_views.logout_fn, name='logout'),
    path('for-doctor', main_views.for_doctor, name='for-doctor'),
    path('for-hospital', main_views.for_hospital, name='for-hospital'),
    path('dashboard/', main_views.dashboard, name='dashboard'),
    path('admin/', admin.site.urls),
]
